//
//  List.swift
//  AmityTodoTechTask
//
//  Created by Apple on 07/11/20.
//

import Foundation

struct ToDo:Decodable {
    var id:String?
    var title:String?
    var description:String?
    var userId:String?
    var createdAt:String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case description
        case userId
        case createdAt
    }
    
    init(id:String, title:String, description:String, userId:String, createdAt:String){
        self.id = id
        self.title = title
        self.description = description
        self.userId = userId
        self.createdAt = createdAt
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(String.self, forKey: .id) ?? ""
        title = try container.decodeIfPresent(String.self, forKey: .title) ?? ""
        description = try container.decodeIfPresent(String.self, forKey: .description) ?? ""
        userId = try container.decodeIfPresent(String.self, forKey: .userId) ?? ""
        createdAt = try container.decodeIfPresent(String.self, forKey: .createdAt) ?? ""
    }
}
