//
//  ToDoList1.swift
//  AmityTodoTechTask
//
//  Created by Apple on 07/11/20.
//

import Foundation

struct ToDoList:Decodable {
    var todo:[ToDo]?
    
    func sortByCreatedAt(_ toDos:[ToDo]) -> [ToDo]? {
        let toDoList = toDos.sorted {
            $0.createdAt! < $1.createdAt!
        }
        return toDoList
    }
}

