//
//  ToDoView.swift
//  AmityTodoTechTask
//
//  Created by Apple on 07/11/20.
//

import Foundation
import UIKit

class ToDoView:UIView {
    
    @IBOutlet weak var toDoTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var toDos:[ToDo]?
    var toDosCopy:[ToDo]?
    var cellSelectionCompletion:((_ cell:IndexPath)->())?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupFromXib()
        setupDelegates()
    }

    func setupFromXib() {
        let view = Bundle.main.loadNibNamed("ToDoView", owner: self, options: nil)?.first as! UIView
        self.addSubview(view)
        self.translatesAutoresizingMaskIntoConstraints = false
        
        setupTableView()
    }
    
    func setupTableView() {
        toDoTableView.delegate = self
        toDoTableView.dataSource = self
        toDoTableView.register(UINib(nibName: "ToDoTableViewCell", bundle: nil), forCellReuseIdentifier: "toDoCell")
    }
    
    func setupDelegates() {
        searchBar.delegate = self
        searchBar.autocapitalizationType = .none
    }
    
    func refereshData() {
        DispatchQueue.main.async {
            self.toDoTableView.reloadData()
        }
    }
    
    func sortListAndReload() {
        self.toDos = Utils.shared.sortByCreatedAt(self.toDos!)
        self.refereshData()
    }
    
    func loadArticlesOnSearchKeyword(keyword:String) {
        if keyword.count != 0 {
            self.toDos = self.toDosCopy?.filter({ (todo) -> Bool in
                todo.title?.contains(keyword) ?? false
            })
        }
        self.sortListAndReload()
    }
}

extension ToDoView:UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count == 0{
            self.toDos = self.toDosCopy
            refereshData()
            return
        }
        loadArticlesOnSearchKeyword(keyword: searchText)
    }
}

extension ToDoView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.toDos?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "toDoCell", for: indexPath) as? ToDoTableViewCell {
            
            cell.setupCell(image: nil, authorTitle: self.toDos?[indexPath.row].title ?? "", dateOfPublish: self.toDos?[indexPath.row].createdAt ?? "")
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.cellSelectionCompletion?(indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
