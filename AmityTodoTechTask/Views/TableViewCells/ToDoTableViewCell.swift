//
//  ToDoiTableViewCell.swift
//  AmityTodoTechTask
//
//  Created by Apple on 07/11/20.
//

import UIKit

class ToDoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var authorTitle: UILabel!
    @IBOutlet weak var dateOfPublish: UILabel!
    @IBOutlet weak var authorImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupCell(image:UIImage?, authorTitle:String, dateOfPublish:String) {
        DispatchQueue.main.async {
            self.authorTitle.text = authorTitle
            let date = Utils.getFormattedDate(string: dateOfPublish)
            self.dateOfPublish.text = date
        }
    }
    
}
