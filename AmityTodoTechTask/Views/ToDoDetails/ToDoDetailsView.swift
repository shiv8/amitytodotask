//
//  ToDoDetailsView.swift
//  AmityTodoTechTask
//
//  Created by Apple on 11/11/20.
//

import Foundation
import UIKit

class ToDoDetailsView:UIView {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var publishedDate: UILabel!
    @IBOutlet weak var todoDescription: UITextView!
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupFromXib()
    }
    
    private func setupFromXib() {
        let view = Bundle.main.loadNibNamed("ToDoDetailsView", owner: self, options: nil)?.first as! UIView
        self.addSubview(view)
        self.translatesAutoresizingMaskIntoConstraints = false
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        view.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
    func setupView(title:String, publishedDate:String, todoDescription:String) {
        DispatchQueue.main.async {
            self.title.text = title
            let date = Utils.getFormattedDate(string: publishedDate)
            self.publishedDate.text = date
            self.todoDescription.text = todoDescription
        }
    }
    
}
