//
//  Utils.swift
//  AmityTodoTechTask
//
//  Created by Apple on 11/11/20.
//

import Foundation

public enum UserDefaultsKeys {
    case amityFile
    case other
    
    func getFileExistsKey() -> String {
        switch self {
        case .amityFile:
            return "toDoFileExists"
        case .other:
            return ""
        }
    }
}

class Utils:NSObject {
    static let shared:Utils = Utils()
    
    static let defaultDateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    
    func checkFileExistsFromUserDefaults(keyName:String) -> Bool {
        return (UserDefaults.standard.value(forKey: keyName) as? String) != nil
    }
    
    func checkFileExists() -> Bool {
        return FileManager.default.fileExists(atPath: filePathUrl.absoluteString)
    }
    
    static func getFormattedDate(string: String) -> String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = defaultDateFormat

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM dd,yyyy"

        let date:Date? = dateFormatterGet.date(from: string)
        print("Date", dateFormatterPrint.string(from: date!))
        return dateFormatterPrint.string(from: date!);
    }
    
    func sortByCreatedAt(_ toDos:[ToDo]) -> [ToDo]? {
        let toDoList = toDos.sorted {
            $0.createdAt! < $1.createdAt!
        }
        return toDoList
    }
}
