//
//  DateManager.swift
//  AmityTodoTechTask
//
//  Created by Apple on 07/11/20.
//

import Foundation

public let filePathUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("amity_task.json")

enum FileErrorCodes {
    case code_101
    case code_102
    
    func getErrorCode() -> String {
        switch self {
        case .code_101:
            return "No data at specified path."
        case .code_102:
            return "Error decoding data."
        }
    }
}

class DataManager:NSObject {
    
    static let shared:DataManager = DataManager()
    
    override init() {
        super.init()
    }
    
    func fetchTodoLists(completionBlock:(@escaping (_ toDos:ToDoList)->())) {
        let userDefaultsKey = UserDefaultsKeys.amityFile.getFileExistsKey()
        let fileAvailable = UserDefaults.standard.value(forKey: userDefaultsKey) as? Bool
        
        guard fileAvailable != nil && fileAvailable == true else {
            ApiManager.shared.fetchLists { (result, error) in
                completionBlock(result ?? ToDoList(todo: []))
            }
            return
        }
        
        getDataFromDocumentsDirectory { (result, error) in
            completionBlock(result ?? ToDoList(todo: []))
        }
    }
    
    func saveDataToDocumentsDirectory(data: Data?) {
        guard data != nil else {
            return
        }
        print("data is \(data?.debugDescription ?? "nil")")
        
        do {
            try data?.write(to: filePathUrl, options: .atomic)
            do {
                let writtenData = try Data(contentsOf: filePathUrl)
                self.checkData(fileData: writtenData)
            } catch let error as NSError {
                print("no data written \(error.debugDescription)")
                return
            }
            
        } catch let error as NSError {
            print("error writing data \(error.debugDescription)")
            return
        }
    }
    
    func checkData(fileData: Data?){
        guard let data = fileData, fileData != nil else {
            return
        }
        
        do {
            let _ = try JSONDecoder().decode(ToDoList.self, from: data)
            self.savedDataKeyForUserDefault()
                
        } catch let error as NSError {
            print("error is \(error.debugDescription)")
        }
    }
    
    func savedDataKeyForUserDefault() {
        UserDefaults.standard.setValue(true, forKey: UserDefaultsKeys.amityFile.getFileExistsKey())
    }
    
    func getDataFromDocumentsDirectory(completionBlock:@escaping (_ toDoList:ToDoList?, _ error:NSError?) -> ()) {
        
        do {
            let writtenData = try Data(contentsOf: filePathUrl)
            do {
                let result = try JSONDecoder().decode(ToDoList.self, from: writtenData)
                completionBlock(result, nil)
                
            } catch let error as NSError {
                completionBlock(nil, error)
            }
            
        } catch let error as NSError {
            completionBlock(nil, error)
        }
    }
}
