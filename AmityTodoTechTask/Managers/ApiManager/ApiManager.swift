//
//  ApiManager.swift
//  AmityTodoTechTask
//
//  Created by Apple on 07/11/20.
//

import Foundation

typealias apiCompletionHandler = (ToDoList?, Error?) -> Void

// Change json file endpoint here.

enum Endpoints {
    case toDoList
    case other
    
    func getUrl() -> String {
        switch self {
        case .toDoList:
            return "https://gist.githubusercontent.com/nRewik/c72c08e6044940c970b742cc13066f4c/raw/e2de4a30232c161abbd3fde69cce405f2e335f89/todos_1k_and_users_50.json"
        case .other:
            return ""
        }
    }
}

class ApiManager:NSObject {
    static let shared:ApiManager = ApiManager()
    
    override init() {
        super.init()
    }
    
    func fetchLists(completionBlock:@escaping apiCompletionHandler) {
        
        let defaultSession = URLSession(configuration: .default)
        var dataTask: URLSessionDataTask?
        
        dataTask?.cancel()
        
        if let urlComponents = URLComponents(string: Endpoints.toDoList.getUrl()) {
            
            guard let url = urlComponents.url else {
                return
            }
            
            dataTask = defaultSession.dataTask(with: url) { [unowned self] (data, response, error) in
                
                if let error = error {
                    completionBlock(nil, error)
                } else {
                    
                    if let data = data, let response = response as? HTTPURLResponse,
                        response.statusCode == 200 {

                        do {
                            let result = try JSONDecoder().decode(ToDoList.self, from: data)
                            
                            DataManager.shared.saveDataToDocumentsDirectory(data: data)
                            
                            DispatchQueue.main.async {
                                completionBlock(result, nil)
                            }
                        } catch let error as NSError {
                            print("Decode Error: \(error.debugDescription)")
                            completionBlock(nil, error)
                            return
                        }
                    }
                    
                    DispatchQueue.main.async {
                        completionBlock(nil, error)
                        return
                    }
                }
            }
            
            dataTask?.resume()
        }
        
    }
    
}
