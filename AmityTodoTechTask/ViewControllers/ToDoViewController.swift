//
//  ViewController.swift
//  AmityTodoTechTask
//
//  Created by Apple on 06/11/20.
//

import UIKit

class ToDoViewController: UIViewController {
    
    @IBOutlet weak var toDoListView: ToDoView!

    override func viewDidLoad() {
        super.viewDidLoad()
        loadInitialData()
        additionalSetups()
    }
    
    func loadInitialData() {
        DataManager.shared.fetchTodoLists { (result) in
            if result.todo?.count == 0 {
                self.showErrorPopUp()
                return
            }
            
            let toDos = result.sortByCreatedAt(result.todo!)
            self.toDoListView.toDos = toDos
            self.toDoListView.toDosCopy = toDos
            self.toDoListView.refereshData()
        }
    }
    
    func additionalSetups() {
        self.toDoListView.cellSelectionCompletion = { [weak self] (cell) in
            self?.navigateToDetailsViewController(cell: cell)
        }
    }
    
    func navigateToDetailsViewController(cell:IndexPath) {
        DispatchQueue.main.async {
            let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "ToDoDetailsViewController") as! ToDoDetailsViewController
            detailsVc.toDo = self.toDoListView.toDos?[cell.row]
            self.navigationController?.pushViewController(detailsVc, animated: true)
        }
    }
    
    func showErrorPopUp() {
        print("some thing error..")
    }
    
}
