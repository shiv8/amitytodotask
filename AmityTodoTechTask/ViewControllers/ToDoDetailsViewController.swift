//
//  ToDoDetailsViewController.swift
//  AmityTodoTechTask
//
//  Created by Apple on 11/11/20.
//

import UIKit

class ToDoDetailsViewController: UIViewController {
    
    @IBOutlet weak var toDoView: ToDoDetailsView!
    
    var toDo:ToDo?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView() {
        toDoView.setupView(title: toDo?.title ?? "", publishedDate: toDo?.createdAt ?? "", todoDescription: toDo?.description ?? "")
    }

}
