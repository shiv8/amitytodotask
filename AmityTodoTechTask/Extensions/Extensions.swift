//
//  Extensions.swift
//  AmityTodoTechTask
//
//  Created by Apple on 11/11/20.
//

import Foundation

extension Date {
   func getFormattedDate(format: String) -> String {
        let dateformat = DateFormatter()
        dateformat.dateFormat = format
        return dateformat.string(from: self)
    }
    
    static func getFormattedDate(string: String , formatter:String) -> String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM dd,yyyy"

        let date:Date? = dateFormatterGet.date(from: string)
        print("Date", dateFormatterPrint.string(from: date!))
        return dateFormatterPrint.string(from: date!);
    }
}
